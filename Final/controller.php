<?php

header('Content-type: application/json');

if($_REQUEST['q'] == 'programador')
{
    $jsn = ['text' => "Cuando tenia 14 años quería hacer un videojuego, a los 16 descubrí linux, me apasiona el software libre."];
} elseif($_REQUEST['q'] == 'expectativas')
{
    $jsn = [
        "Demostrar de lo que soy capaz",
        "Aprender",
        "Trabajar en equipo"
        ];
} elseif($_REQUEST['q'] == 'rasgos')
{
    $jsn = [
        ["PHP 7 OPP", 5],
        ["Javascript + Ajax ", 4],
        ["CSS + Less", 5],
        ["HTML 5", 5],
        ["Docker", 3],
        ["Linux", 5],
        ["Laravel", 2],
        ["KumbiaPHP", 5],
        ["MySql", 3]
    ];
}

$jsnstr = json_encode($jsn);

echo $jsnstr;