addEventListener('load', initEvents, false);
var result;
var conexion1;
var value;

function initEvents()
{
    var elements = document.getElementsByClassName("content");
    for(var j=0; j<elements.length; j++)
    {
        elements[j].addEventListener('click', queryLaunch, false);
    }
}

function queryLaunch(e)
{
    value = e.currentTarget.id;
    result = document.getElementById("result-"+value);
    var ref = document.getElementById(value);
    var uri="controller.php?q="+value;
    ref.classList.add("selected");
    getInfo(uri);
}

function getInfo(uri)
{
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarDatos;
    conexion1.open("GET", uri, true);
    conexion1.send();
}

function procesarDatos()
{
    var inObj;
    if(conexion1.readyState == 4)
    {
        inObj = JSON.parse(conexion1.responseText);
        render(inObj);
    } else {
        result.innerHTML = "cargando";
    }
}

function render(inObj)
{
    switch(value)
        {
            case "programador":
                result.innerHTML = "<p>" + inObj.text + "</p>";
                clearResults();
                break;

            case "expectativas":
                result.innerHTML = getExpc(inObj);
                clearResults();
                break;

            case "rasgos":
                result.innerHTML = getRasg(inObj);
                clearResults();
                break;
        }
}

function clearResults()
{
    var elements = document.getElementsByClassName("content");
    for(var j=0; j<elements.length; j++)
    {
        if(elements[j].id == value)
        {
            //nothing
        }
        else
        {
            elements[j].childNodes[3].innerHTML = "" ;
            elements[j].classList.remove("selected");
        }
    }
}

function getExpc(obj)
{
    var tmpStr = "<ul>";
    for(var j=0; j<obj.length; j++)
    {
        tmpStr = tmpStr.concat("<li>" + obj[j]+"</li>");
    }
    tmpStr = tmpStr.concat("</ul>");
    return tmpStr;
}

function getRasg(obj)
{
    var tmpStr = "<ul>";
    for(var j=0; j<obj.length; j++)
    {
        tmpStr = tmpStr.concat("<li>" + obj[j][0])
        tmpStr = tmpStr.concat('<img alt="' + obj[j][1] +'" src="media/'+ obj[j][1] +'.svg">');
        tmpStr = tmpStr.concat("</li>");
    }
    return tmpStr;
}